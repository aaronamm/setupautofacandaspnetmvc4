﻿using System;
using System.ComponentModel;
using System.Reflection;
using Autofac;
using SetupAutofac.BLL;
using SetupAutofac.Model; 
using SetupAutofac.Model;
using SetupAutoface.Repository.NH;

namespace SetupAutofac.ConsoleApp
{
    class Program
    {

        private static Autofac.IContainer _container;
        static void Main(string[] args)
        {

            var builder =  new ContainerBuilder();
            //specific type
            builder.RegisterType<UserRepository>().As<IUserRepository>();

            //inject to property of service class
            var businessLayer = Assembly.GetAssembly(typeof(UserService));
            builder.RegisterAssemblyTypes(businessLayer)
                .Where(t =>t.Name.EndsWith("Service"))
                .PropertiesAutowired();

             _container = builder.Build();

             ListAllUsers();
        }

        public static void ListAllUsers()
        {
            // Create the scope, resolve your IUserRepository
            // use it, then dispose of the scope.
            using (var scope = _container.BeginLifetimeScope())
            {
                var userService = scope.Resolve<UserService>();
                var users = userService.GetAllUsers();

                foreach(var user in users)
                {
                    Console.WriteLine("user id: {0}, name: {1}", user.Id, user.Name);
                }
            }
        }


    }
}
