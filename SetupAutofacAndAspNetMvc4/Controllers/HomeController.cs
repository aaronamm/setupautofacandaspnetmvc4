﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SetupAutofac.Model;

namespace SetupAutofacAndAspNetMvc4.Controllers
{
    public class HomeController : Controller
    {
        private IUserRepository _userRepository;
        public HomeController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        //
        // GET: /Home/

        public ActionResult Index()
        {
            var users = _userRepository.FindAll();
            return View(users);
        }

    }
}
