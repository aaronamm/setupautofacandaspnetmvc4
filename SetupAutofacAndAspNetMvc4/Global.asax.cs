﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using SetupAutofacAndAspNetMvc4.App_Start;
using System.Reflection;

namespace SetupAutofac.Mvc4
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly).InstancePerHttpRequest();

            //specific type
            //builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerHttpRequest();


            //auto scan
            var dataAccess = Assembly.GetExecutingAssembly();
            builder.RegisterAssemblyTypes(dataAccess)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerHttpRequest();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            // Other MVC setup...
           
        }

        //ref
        //http://stackoverflow.com/questions/12390665/mvc-4-autofac-and-generic-repository-pattern
        //http://stackoverflow.com/questions/10336983/mvc-3-generic-repositories-injection-with-autofac
        //http://www.codeproject.com/Articles/562871/MVC-Repository-Pattern-with-Entity-Framework-and-s
        //http://forums.asp.net/t/1797395.aspx?MVC+3+Generic+Repositories+Injection+with+Autofac

        //http://code.google.com/p/autofac/wiki/Scanning
        //https://www.nuget.org/packages/Autofac.Mvc4/
        //http://code.google.com/p/autofac/wiki/GettingStarted
        //http://www.codeproject.com/Articles/25380/Dependency-Injection-with-Autofac
       //download
        //https://www.nuget.org/packages/Autofac.Mvc4/
	//http://www.claassen.net/geek/blog/2009/08/setting-up-mocks-for-inner-autofac.html


    }
}
