﻿using System.Web.Mvc;

namespace SetupAutofacAndAspNetMvc4.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}