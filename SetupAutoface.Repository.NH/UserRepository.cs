﻿using System.Collections.Generic;
using System.Linq;
using SetupAutofac.Model;

namespace SetupAutoface.Repository.NH
{
    public class UserRepository:IUserRepository
    {

        private List<User> userList = new List<User>();
        public UserRepository()
        {

            userList.Add(new User() { Id = 1, Name = "Nine" });
            userList.Add(new User() { Id = 2, Name = "Non" });
            userList.Add(new User() { Id = 3, Name = "Amm" });
        }

        public User FindById (int id)
        {
            return userList.SingleOrDefault(u => u.Id == id);
        }


        public IEnumerable<User> FindAll()
        {

            return userList;
        }
    }
}