﻿using System.Collections.Generic;

namespace SetupAutofac.Model
{
    public interface IUserRepository
    {
         User FindById(int id);

         IEnumerable<User> FindAll();
    }
}