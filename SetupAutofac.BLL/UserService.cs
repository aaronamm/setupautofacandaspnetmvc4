﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SetupAutofac.Model;

namespace SetupAutofac.BLL
{
    public class UserService 
    {
        public IUserRepository UserRepository { get; set; }

        public IEnumerable<User> GetAllUsers()
        {
        return    UserRepository.FindAll();
        }

    }
}
